/**
 * Copyright (C) 2012 by Justin Windle
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// interactive - squid follows mouse at all time
// demo - true allows user to click and have the squid follow the mouse
    // gravity and wind go -3 to 3, music_notes go up to 100

var settings = {
        interactive: false,
        darkTheme: true,
        headRadius: 60,
        thickness: 18,
        music_notes: 15,
        friction: 0.02,
        gravity: 0.5,
        colour: {h: 0, s: 0, v: 0.1},
        length: 70,
        pulse: true,
        wind: -0.5,
    };

var demo = true;
var ease = 0.1;
var modified = true;
var radius = settings.headRadius;
var music_notes = [];
var center = {x: 0, y: 0};
var scale = window.devicePixelRatio || 1;
var sketch = Sketch.create({

    retina: 'auto',

    container: document.getElementById('music_notes'),

    setup: function () {

        center.x = this.width / 2;
        center.y = this.height / 2;
        center.x = -50;
        center.y = 0;
    },

    update: function () {
        var t, cx, cy, pulse;

        t = this.millis * 0.001;

        t = this.millis;
        cx = this.width * 0.5;
        cy = this.height * 0.5;

        center.x = center.x + 20;
        center.y = cy + sin(t * 0.003) * tan(sin(t * 0.0003) * 1.15) * cy * 0.4;
        if (center.x > $(window).width() + 500) {
            center.x = 0;
        }
    },

    draw: function () {
        var h = settings.colour.h * 0.95;
        var s = settings.colour.s * 100 * 0.95;
        var v = settings.colour.v * 100 * 0.95;
        var w = v + (settings.darkTheme ? -10 : 10);

        for (var i=-15; i<15; i++) {
            var diffx = i * 350;
            var diffy = i * 250;
            this.beginPath();
            this.arc(center.x + diffx, center.y + diffy, 10, 0, TWO_PI);
            this.fillStyle = 'hsl(' + h + ',' + s + '%,' + v + '%)';
            this.fill();
            this.arc(center.x + diffx + 50, center.y + diffy, 10, 0, TWO_PI);
            this.fillStyle = 'hsl(' + h + ',' + s + '%,' + v + '%)';
            this.fill();
            this.rect(center.x + diffx, center.y + diffy - 50, 10, 50);
            this.fillStyle = 'hsl(' + h + ',' + s + '%,' + v + '%)';
            this.fill();
            this.rect(center.x + diffx + 50, center.y + diffy - 50, 10, 50);
            this.fillStyle = 'hsl(' + h + ',' + s + '%,' + v + '%)';
            this.fill();
            this.rect(center.x + diffx, center.y + diffy - 50, 50, 10);
            this.fillStyle = 'hsl(' + h + ',' + s + '%,' + v + '%)';
            this.fill();
            this.closePath();
        }
    },

    export: function () {
        window.open(this.canvas.toDataURL(), 'music_notes', "top=20,left=20,width=" + this.width + ",height=" + this.height);
    }
});

function onSettingsChanged() {
    modified = true;
}

function onThemeChanged(dark) {

    settings.colour.h = 0;
    settings.colour.s = 0;
    settings.colour.v = dark ? 0.8 : 0.1;

    document.body.className = dark ? 'dark' : '';

    colourGUI.updateDisplay();
}


onThemeChanged(true);