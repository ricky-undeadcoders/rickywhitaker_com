var page_state = "home";
var squid_state = "on";
make_squid();

function hide_all() {
    $('#programmer-btn').fadeOut('slow');
    $('#musician-btn').fadeOut('slow');
    $('#header').fadeOut('slow');
}

function toggle_squid() {
    var stop_button = $('#stop-btn');
    stop_button.css('box-shadow', 'inset 0 0 white;');
    if (squid_state === "on") {
        squid_state = "off";
        stop_button.css("background", "green");
        $('#squidward').html('');
        stop_button.text('Go');
    } else {
        squid_state = "on";
        stop_button.text('Stop');
        stop_button.css("background", "red");
        if (page_state === "musician") {
            make_squid_and_music();
        } else if (page_state === "programmer") {
            make_snake();
        } else {
            make_squid();
        }
    }
}

function reset() {
    page_state = "home";
    $('#header').fadeIn('slow');
    $('.text-boxes').fadeOut('slow');
    $('.text-boxes').css('top', '85%');
    $('#programmer-text').css('left', '-70%');
    $('#musician-text').css('left', '85%');
    // $('.text-boxes').addClass('hidden');
    $('#programmer-btn').fadeIn('slow');
    $('#musician-btn').fadeIn('slow');
    $('#squidward').html('');
    if (squid_state === "on") {
        make_squid();
    }
}

function click_programmer_btn() {
    page_state = "programmer";
    hide_all();
    $('#squidward').html('');
    if (squid_state === "on") {
        make_snake();
    }
    var text_div = $('#programmer-text');
    text_div.fadeIn('slow');
    text_div.removeClass('hidden');
    text_div.css('position', 'fixed');
    text_div.css('top', 0);
    text_div.css('left', 0);
    text_div.css('opacity', 0.8);
    text_div.css('height', '100%');
    text_div.css('width', '100%');
    text_div.css('background', '#2F4F4F');
}

function click_musician_btn() {
    page_state = "musician";
    hide_all();
    $('#squidward').html('');
    if (squid_state === "on") {
        make_squid_and_music();
    }
    var text_div = $('#musician-text');
    text_div.fadeIn('slow');
    text_div.removeClass('hidden');
    text_div.css('position', 'fixed');
    text_div.css('top', 0);
    text_div.css('left', 0);
    text_div.css('opacity', 0.8);
    text_div.css('height', '100%');
    text_div.css('width', '100%');
    text_div.css('background', 'black');
}

$('#musician-btn').click(function () {
    click_musician_btn();
});

$('#programmer-btn').click(function () {
    click_programmer_btn();
});